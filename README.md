# 学习来源
https://space.bilibili.com/370283072/channel/detail?cid=95088&ctype=0

# 素材来源

## 模型包
* [Asset store] Dog Knight PBR Polyart
* [Asset store] Low-Poly Simple Nature Pack
* [Asset store] RPG Monster Duo PBR Polyart
* [Asset store] Mini Legion Grunt PBR HP Polyart
* [Asset store] Mini Legion Rock Golem PBR HP Polyart

## 天空盒素材
* [Asset store] Skybox Extended Shader

# 使用的插件说明

## CineMachine
实现相机跟随

## PostProcessing
场景效果

## Poly Brush
多边形凹凸地形，比Plane功能强大。可使用模型、贴图笔刷，无需一个一个往场景里放模型或贴图，提高工作效率。

## ProGrid (Preview package)
标尺，可让场景物体按照既定尺寸单元去移动

## Pro Builder
快速建模，一般搭配Poly Brush使用

## Nav Mesh
导航网格，AI自动寻路

# 快捷键技巧

## Ctrl + Shift + F
选中相机，按下快捷键，以当前视图角度作为相机的视角。
GameObject -> Align With View

将视角切换为相机视角：
GameObject -> Align View to Selected

## Right(Mouse) + W/S/A/D/Q/E
玩家视角前后左右上下移动

# 小技巧

## 3D场景鼠标点击控制角色移动

```C#
using UnityEngine;
using System;

public class MouseManager : MonoBehaviour
{
    RaycastHit hitInfo;
    public event Action<Vector3> OnMouseClicked;
    
    private void Update()
    {
        SetCursorTexture();
        MouseControl();
    }

    void SetCursorTexture()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out hitInfo);
    }

    void MouseControl()
    {
        if (Input.GetMouseButtonDown(0) && hitInfo.collider != null)
        {
            if (hitInfo.collider.gameObject.CompareTag("Ground"))
            {
                OnMouseClicked?.Invoke(hitInfo.point);
            }
        }
    }
}

using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    private NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    private void Start()
    {
        MouseManager.Instance.OnMouseClicked += MoveToTarget;
    }

    public void MoveToTarget(Vector3 target)
    {
        agent.destination = target;
    }
}
```

## 替换鼠标贴图
```C#
using System;

// 贴图需要设置为Cursor类型，而不是Sprite
// 贴图，偏移，模式
Cursor.SetCursor(texture2D, new Vector2(16, 16), CursorMode.Auto);
```

## Blend Tree
通过变量来控制不同状态播放不同动画，比如：根据跑动的速度不同，动画有停留变为慢跑，在变为快跑。使用BlendTree使动画过渡更加柔和，一般在多个动画之间切换中使用。

## Shader Graph 遮挡剔除

视频链接：https://www.bilibili.com/video/BV1i5411P7bd

创建Unlit Graph及其对应的Material，在Occlusion Shader里面设置Color、DitherCount、AlphaThreshold，实现角色被遮挡时的材质效果。

在UniversalRenderPipelineAsset_Renderer里Add Renderer Feature，设置Occlusion材质，设置Filter/Layer Mask为遮挡物的Layer，并设置Depth Test为Greater，表示遮挡物在角色前面时，显示Occlusion的材质。

## 挂载脚本时自动添加需要组件

在脚本类前面添加代码：
```C#
[RequireComponent(typeof(NavMeshAgent))]
public class EnemyController : MonoBehaviour
{
    private NavMeshAgent agent;
}

```

## 判断周围范围内有没有物体

```C#
using UnityEngine;

var colliders = Physics.OverlapSphere(transform.position, sightRadius);
```

## Gizmos画线
```C#
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, sightRadius);
    }
```

## 防止NPC随机走动到不可行走的区域
```C#
NavMeshHit hit;
wayPoint = NavMesh.SamplePosition(randomPoint, out hit, patrolRange, 1) ? hit.position : transform.position;
```

## Scriptable作用
角色、物品属性通用数值模板。

## C# Properties
```C#
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStates : MonoBehaviour
{
    public CharacterData_SO characterData;

    public int MaxHealth { get; set; }
    
    // 扩展用法
    public int MaxHealth
    {
        get
        {
            if (characterData != null)
                return characterData.maxHealth;
            else
                return 0;
        }

        set
        {
            // value为内置变量
            characterData.maxHealth = value;
        }
    }
}
```

## C# #region的用法
region标签用户将region和endregion之间的代码折叠收齐，同时可添加备注说明
```C#
public class CharacterStates : MonoBehaviour
{
    public CharacterData_SO characterData;

    #region Read from Data_SO
    public int MaxHealth
    {
        get { if (characterData != null) return characterData.maxHealth; else return 0; }
        set { characterData.maxHealth = value; }
    }

    public int CurrentHealth
    {
        get { if (characterData != null) return characterData.currentHealth; else return 0; }
        set { characterData.currentHealth = value; }
    }

    public int BaseDefence
    {
        get { if (characterData != null) return characterData.baseDefence; else return 0; }
        set { characterData.baseDefence = value; }
    }

    public int CurrentDefence
    {
        get { if (characterData != null) return characterData.currentDefence; else return 0; }
        set { characterData.currentDefence = value; }
    }
    #endregion
}
```

## 第三方的Animation Read-Only，无法编辑的解决方法

将第三方FBX的animation拷贝一份，替换到自己用的Prefab里的Animator中，再编辑拷贝的那份animation。

## 泛型单例
```C#
public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    private static T instance;

    public static T Instance
    {
        get { return instance; }
    }

    protected virtual void Awake()
    {
        if (instance != null)
            Destroy(gameObject);
        else
            instance = (T)this;
    }

    public static bool IsInitialized
    {
        get { return instance != null; }
    }

    protected virtual void OnDestroy()
    {
        if (instance == this)
        {
            instance = null;
        }
    }
}
```

## Unity文件夹展开、折叠快捷键
选中某个文件夹，按左：折叠，按右：展开。

## Unity暂停快捷键
Ctrl + Shift + P